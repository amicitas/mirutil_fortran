


! ======================================================================
!+
!
! Setup error types for cerfit.
! 
!-
! ======================================================================
      MODULE error_cerfit
        IMPLICIT NONE


        ! --------------------------------------------------------------
        ! Errors due to user interaciton
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorUserExit = 'errorUserExit'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorUserCommandBad = 'errorUserCommandBad'


        ! --------------------------------------------------------------
        ! Shot related errors
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorShotNotFound = 'errorShotNotFound'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorShotInvalid = 'errorShotInvalid'


        ! --------------------------------------------------------------
        ! Chord related erros
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorChordInvalid = 'errorChordInvalid'


        ! --------------------------------------------------------------
        ! Beam related errors
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorBeamInvalid = 'errorBeamInvalid'


        ! --------------------------------------------------------------
        ! Profile related errors
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorProfileNotFound = 'errorProfileNotFound'


        ! --------------------------------------------------------------
        ! Calibration related errors
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorCalibrationNotFound = 'errorCalibrationNotFound'


        ! --------------------------------------------------------------
        ! Set errors for reading data files.
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorRead = 'errorRead'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorReadBufferOverflow = 'errorReadBufferOverflow'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorReadDataOverflow = 'errorReadDataOverflow'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorReadEndOfShot = 'errorReadEndOfShot'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorReadNoDataFound = 'errorReadNoDataFound'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorReadSomeDataFound = 'errorReadSomeDataFound'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorReadInfiniteLoop = 'errorReadInfiniteLoop'


        ! --------------------------------------------------------------
        ! Set errors for string handling
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorStringParse = 'errorStringParse'

      END MODULE error_cerfit
