
! ======================================================================
!+
!
! AUTHOR:
!   Novimir Antoniuk Pablant
!
! DATE:
!   2009-05-13
!
! PURPOSE:
!   This is a module to use for error handling.
!
! DESCRIPTION:
!   This module maintains an error status and an error stack.
!   The error stack is a list of every error that has occured.
!
!   To check the error status the funciton ERROR() can be used.
!   To add an error to the error stack the subroutine SET_ERROR()
!     can be used.
!   To reset the error status and clear the error stack the subroutine
!     CLEAR_ERROR() can be used.
!
!   Additional routines are described below.
!
!   This module also provides a convenience varible:
!     m_error_string
!
!   This variable can be used to assemble error messages.
!   See the example below.
!
!
!
! BASIC ROUTINES:
!
!   The module has 5 basic routines:
!   
!   SET_ERROR (location, message [,error_type])
!       This subroutine allows the error location and error message to 
!       be added te the error stack.  It will also set the error
!       status to .TRUE.
!
!       Optionally an error_type can also be given.  
!
!       The error type is a string descriptor, and should generally 
!       be defined as as a module variable. The covention for
!       the error type is as follows:
!         'errorCategoryType'
!       So for example a file open error is defined as:
!         'errorFileOpen'
!
!       Many error types are predefined in this module.
!
!   ERROR ()
!       This function will return the error state.
!       It will return .TRUE. if an error has occured.
!
!   CLEAR_ERROR()
!       This will reset the error status to .FALSE. and clear
!       the error stack.
!
!   IS_ERROR_TYPE (error_type)
!       This function will check if the last error encountered is of
!       the type given. If it is then .TRUE. will be returned, otherwise
!       .FALSE. will be returned.
!
!       If the error status is .FALSE. then IS_ERROR_TYPE will always
!       be .FALSE.
!
!       You can also check the first error or all errors, see functions
!       listed below.
!
!   PRINT_ERROR()
!       This subroutine will print the error message for the last error
!       in the error stack.
!
!       The last message or all messages in the stack can also be 
!       printed, see routines listed below.  A debugging mode
!       can also be turned on in which each message will be printed
!       as it is set, see SET_ERROR_DEBUG()
!
!   PRINT_ERROR_TYPE()
!       This subroutine will print the error type for the last error
!       in the error stack.
!
!       
!
!
! ADDITIONAL ROUTINES:
!
!   There are a number of additonal routines that allow the error
!   stack to be examined.
!
!   IS_LAST_ERROR_TYPE (error_type)
!       This is the same as IS_ERROR_TYPE()
!
!   IS_FIRST_ERROR_TYPE (error_type)
!       Will check if the first error encountered is of the type given.
!
!   IS_ANY_ERROR_TYPE (error_type)
!       Will check if the any of the errors in the error stack are of
!       the type given.
!
!   PRINT_LAST_ERROR()
!       This is same as PRINT_ERROR()
!
!   PRINT_FIRST_ERROR()
!       Print the error message from the fist error that was
!       encounterd.
!
!   PRINT_ALL_ERROR()
!       Print all of the errors in the error stack.
!       Errors will be printed from fist encountered to last.
!
!   PRINT_LAST_ERROR_TYPE()
!   PRINT_FIRST_ERROR_TYPE()
!   PRINT_ALL_ERROR_TYPE()
!       These are the same as the PRINT_ERROR routines, but print
!       the error type only.
!   
!   SET_ERROR_DEBUG(debug_mode)
!       This subroutine can be used to set the debug mode of the
!       error module.  If debug_mode is .TRUE., then error messages 
!       will be printed whenever SET_ERROR() is called.
!
!
!   
!
! INTEGER ERRROR CODES:
!
!   The string base error system described above is ideal for
!   clarity in coding, output to the user, and for debugging.
!
!   It can be slow however if used inside of loops with many
!   interations (I don't know how slow though).
!
!   For this reason this module also contains a integer error code
!   system.  This should be avoided unless speed is absolutly needed.
!
!   BASIC ROUTINES:
!  
!     SET_ERROR_CODE(error_code)
!         This subroutine is used to add the given error code to the
!         error stack.  The error status will be set to .TRUE..
!
!         error_code = 0 is means 'no error'.  If this error code
!         is given, no action will be taken.
!
!     ERROR()
!         Same as before.
!   
!     CLEAR_ERROR()
!         Same as before.
!
!     IS_ERROR_CODE(error_code)
!         This function will check if the last error encountered had
!         the given error code. If so .TRUE. will be returned.
!
!         If the error status is .FALSE. then IS_ERROR_CODE will
!         check if the given error_code = 0.
!
!         You can also check the first error or all errors, see functions
!         listed below.
!
!
!     PRINT_ALL_ERROR_CODE()
!         Will print out all error codes in the error stack.
!         Errors will be printed from fist encountered to last.
!     
!
!   ADDITIONAL ROUTINES:
!
!     IS_LAST_ERROR_CODE(error_code)
!         This is the same as IS_ERROR_CODE()
!
!     IS_FIRST_ERROR_CODE(error_code)
!         This will check if the the first error encountered had
!         the given error code.
!
!     IS_ANY_ERROR_CODE(error_code)
!         This will check if any error in the error stack had
!         the given error code.
! 
!
!
! EXAMPLE:
!
! ! Here is an example of a module that make use of the error module.
!
! MODULE test_module
!   ! First we use the error module.
!   USE error_module
!
!   ! Now lets define some custom error types
!   CHARACTER(LEN=*), PARAMETER :: errorTestBad
!   CHARACTER(LET=*), PARAMETER :: errorTestWarning
!
! CONTAINS
!
! ! Assume that there are other routines in here too.
!
! SUBROUTINE SHOWOFF()
!
!   ! Now lets call another routine that uses the error module.
!   CALL MISBEHAVING_ROUTINE()
!
!   ! We can now check whether there was an error in that routine.
!   IF (ERROR()) THEN
!     ! At this point we could simply 'RETURN' and let some routine
!     ! higher up deal with the error.
!
!     ! Lets instead try to handle the error.
!     ! Lets check the error type an see if it is important.
!     IF (IS_ERROR_TYPE(errorTestWarning) THEN
!        ! This wast just a warning.  Lets print and continue.
!        CALL PRINT_ERROR()
!        ! Make sure to clear the error stack.
!        CALL CLEAR_ERROR()
!
!     ELSE IF (IS_ERROR_TYPE(errorTestBad)) THEN
!        ! We can't recover from this one.
!        ! Lets take some action and then add a new error to the stack.
!        . . . taking action
!        CALL SET_WARNING('SHOWOFF', 'Something bad happened.', errorTestWarning)
!        RETURN
!     ENDIF
!   ENDIF
!
!   ! Now for an example of using m_error_string.
!   ! Let say that we encounter an error with an index being out 
!   ! of range (boring I know).  Instead of declaring a variable
!   ! just to output the error message we do this:
!   WRITE(m_error_string, '(a,i0)') 'I found an out of range index: ', index
!   CALL SET_ERROR('SHOWOFF', m_error_string)
!
!   ! Well that shows off some of the error_module.  Hope it is usefull
!   ! To you.
! END SUBROUTINE SHOWOFF
! END MODULE test_module
!
!   
!        
!     
!-
! ======================================================================
!
!=======================================================================
!=======================================================================
      MODULE error_module
        IMPLICIT NONE
        PRIVATE

        ! --------------------------------------------------------------
        ! Define the public functions
        PUBLIC :: ERROR &
                  ,SET_ERROR, SET_ERROR_CODE &
                  ,CLEAR_ERROR &

                  ,IS_ERROR_TYPE, IS_LAST_ERROR_TYPE &
                  ,IS_FIRST_ERROR_TYPE, IS_ANY_ERROR_TYPE &

                  ,IS_ERROR_CODE, IS_LAST_ERROR_CODE &
                  ,IS_FIRST_ERROR_CODE, IS_ANY_ERROR_CODE &

                  ,PRINT_ERROR, PRINT_LAST_ERROR &
                  ,PRINT_FIRST_ERROR, PRINT_ALL_ERROR &

                  ,PRINT_ERROR_TYPE, PRINT_LAST_ERROR_TYPE &
                  ,PRINT_FIRST_ERROR_TYPE, PRINT_ALL_ERROR_TYPE &

                  ,PRINT_ALL_ERROR_CODE &

                  ,SET_ERROR_DEBUG


        ! --------------------------------------------------------------
        ! Setup a debug flag
        LOGICAL, SAVE:: m_error_debug = .FALSE.

        ! --------------------------------------------------------------
        ! Setup the stack size.  For now just use a fixed stack.
        INTEGER, PARAMETER :: m_error_stack_size = 20
        INTEGER, PARAMETER :: m_error_string_len = 100
        
        ! --------------------------------------------------------------
        ! Setup some error handling variables.
        !
        LOGICAL, SAVE :: m_error = .FALSE.
        CHARACTER(LEN=m_error_string_len), DIMENSION(m_error_stack_size), SAVE :: m_error_location
        CHARACTER(LEN=m_error_string_len), DIMENSION(m_error_stack_size), SAVE :: m_error_message
        CHARACTER(LEN=m_error_string_len), DIMENSION(m_error_stack_size), SAVE :: m_error_type
        INTEGER, DIMENSION(m_error_stack_size), SAVE :: m_error_code
        INTEGER, SAVE :: m_stack_position

        ! --------------------------------------------------------------
        ! Some convenience variables.
        CHARACTER(LEN=m_error_string_len), PUBLIC :: m_error_string





        ! --------------------------------------------------------------
        ! Setup standard errors

        ! Error errors.
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorErrorStackFull = 'errorErrorStackFull'

        ! File errors.
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorFileNotFound = 'errorFileNotFound'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorFileNotDefined = 'errorFileNotDefined'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorFileOpen = 'errorFileOpen'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorFileClose = 'errorFileClose'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorFileRead = 'errorFileRead'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorFileWrite = 'errorFileWrite'
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorFileEofEor = 'errorFileEofEor'

        ! Path errors
        CHARACTER (LEN=*), PARAMETER, PUBLIC :: errorPathInvalid = 'errorPathInvalid'


        ! --------------------------------------------------------------
        ! Define standard error codes
        !
        ! -9999 - Error in the error_module. Error stack full.

      CONTAINS




        ! ##############################################################
        !+
        ! Return the error state.
        ! 3/30/11 rjg - declare pure
        !-
        ! ##############################################################
        PURE LOGICAL FUNCTION  ERROR ()
          IMPLICIT NONE

          ERROR = m_error

        END FUNCTION ERROR



        ! ##############################################################
        !+
        ! Clear the error state.
        !-
        ! ##############################################################
        SUBROUTINE CLEAR_ERROR ()
          IMPLICIT NONE
          
          m_error = .FALSE.
          m_stack_position = 0
          m_error_code = 0

        END SUBROUTINE CLEAR_ERROR



        ! ##############################################################
        !+
        ! Create a routine to set errors
        !-
        ! ##############################################################
        SUBROUTINE SET_ERROR (error_location, error_message, error_type)
          IMPLICIT NONE

          CHARACTER(LEN=*), INTENT(IN) :: error_location
          CHARACTER(LEN=*), INTENT(IN) :: error_message
          CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: error_type

          INTEGER :: message_length
          INTEGER :: location_length
          INTEGER :: type_length
          
          m_error = .TRUE.

          ! Increment the number of stack slots used.
          m_stack_position = m_stack_position + 1

          ! Make sure that we do not overflow the stack
          IF (m_stack_position .GT. m_error_stack_size) THEN
             RETURN

          ELSE IF (m_stack_position .EQ. m_error_stack_size) THEN
             m_error_location(m_error_stack_size) = 'SET_ERROR'
             m_error_message(m_error_stack_size) = 'Error stack is full.'
             m_error_type(m_error_stack_size) = errorErrorStackFull


          ELSE

             ! Check for overflows.
             message_length = MIN(LEN(m_error_message), LEN(error_message))
             location_length = MIN(LEN(m_error_location), LEN(error_location))

             m_error_location(m_stack_position) = error_location(1:location_length)
             m_error_message(m_stack_position) = error_message(1:message_length)

             ! Now deal with the error type.
             ! Clear the error type if one was not given.
             IF (PRESENT(error_type)) THEN
                type_length = MIN(LEN(m_error_type), LEN(error_type))
                m_error_type(m_stack_position) = error_type(1:type_length)
             ELSE
                m_error_type(m_stack_position) = ''
             ENDIF
          ENDIF

          IF (m_error_debug) CALL PRINT_LAST_ERROR()
          
        END SUBROUTINE SET_ERROR


        ! ##############################################################
        !+
        ! Create a routine to set error codes.
        !
        ! In generaly it is much better, in terms of code clarity
        ! and debuging, to use SET_ERROR and use error_type.
        !
        ! This is here for cases in which doing string operations
        ! would be too slow (such as in the middle of a fitting loop.)
        !
        ! If the input error code is zero, then no action will be taken.
        !
        ! NOTE: If there is an internal error (stack overflow), the
        !       last error code will be set to -9999.
        !-
        ! ##############################################################
        SUBROUTINE SET_ERROR_CODE (error_code)
          IMPLICIT NONE
          
          INTEGER, INTENT(IN) :: error_code

          IF (error_code .EQ. 0) THEN
             RETURN
          ENDIF

          m_error = .TRUE.

          ! Increment the number of stack slots used.
          m_stack_position = m_stack_position + 1

          ! Make sure that we do not overflow the stack
          IF (m_stack_position .GT. m_error_stack_size) THEN
             RETURN
          ELSE IF (m_stack_position .EQ. m_error_stack_size) THEN
             m_error_code(m_error_stack_size) = -9999
          ELSE
             m_error_code(m_stack_position) = error_code
          ENDIF
          

        END SUBROUTINE SET_ERROR_CODE



        ! ##############################################################
        !+
        ! Compare the input error type with the last error type.
        !
        ! This is a convenience function. It simply wraps:
        !   IS_LAST_ERROR_TYPE
        !-
        ! ##############################################################
        LOGICAL FUNCTION  IS_ERROR_TYPE (error_type)
          IMPLICIT NONE
          
          CHARACTER(LEN=*), INTENT(IN) :: error_type

          IS_ERROR_TYPE = IS_LAST_ERROR_TYPE(error_type)


        END FUNCTION IS_ERROR_TYPE


        ! ##############################################################
        !+
        ! Compare the input error type with the last error type.
        !-
        ! ##############################################################
        LOGICAL FUNCTION  IS_LAST_ERROR_TYPE (error_type)
          IMPLICIT NONE
          
          CHARACTER(LEN=*), INTENT(IN) :: error_type
          IF (m_error) THEN
             IS_LAST_ERROR_TYPE = IS_INDEX_ERROR_TYPE(error_type, m_stack_position)
          ELSE
             IS_LAST_ERROR_TYPE = .FALSE.
          ENDIF

        END FUNCTION IS_LAST_ERROR_TYPE



        ! ##############################################################
        !+
        ! Compare the input error type with the last first type.
        !-
        ! ##############################################################
        LOGICAL FUNCTION  IS_FIRST_ERROR_TYPE (error_type)
          IMPLICIT NONE
          
          CHARACTER(LEN=*), INTENT(IN) :: error_type
          IF (m_error) THEN
             IS_FIRST_ERROR_TYPE = IS_INDEX_ERROR_TYPE(error_type, 1)
          ELSE
             IS_FIRST_ERROR_TYPE = .FALSE.
          ENDIF

        END FUNCTION IS_FIRST_ERROR_TYPE



        ! ##############################################################
        !+
        ! Compare the input error type with any error types in the 
        ! stack.
        !-
        ! ##############################################################
        LOGICAL FUNCTION  IS_ANY_ERROR_TYPE (error_type)
          IMPLICIT NONE
          
          INTEGER :: ii

          CHARACTER(LEN=*), INTENT(IN) :: error_type

          IF (m_error) THEN
             DO ii=1,m_error_stack_size
                IS_ANY_ERROR_TYPE = IS_INDEX_ERROR_TYPE(error_type, ii)
                IF (IS_ANY_ERROR_TYPE) RETURN
             END DO
          ELSE
             IS_ANY_ERROR_TYPE = .FALSE.
          ENDIF

        END FUNCTION IS_ANY_ERROR_TYPE



        ! ##############################################################
        !+
        ! Compare the input error type with the error type at the
        ! given position in the stack.
        !
        ! Note: This will not check the stack position or the
        !       error status.
        !-
        ! ##############################################################
        LOGICAL FUNCTION  IS_INDEX_ERROR_TYPE (error_type, index)
          IMPLICIT NONE
          
          CHARACTER(LEN=*), INTENT(IN) :: error_type
          INTEGER, INTENT(IN) :: index

          IS_INDEX_ERROR_TYPE = (error_type .EQ.  TRIM(m_error_type(index)))

        END FUNCTION IS_INDEX_ERROR_TYPE



        ! ##############################################################
        !+
        ! Compare the input error code with the last error code.
        !
        ! This is a convience funcition.  It simply wraps:
        !   IS_LAST_ERROR_CODE
        !-
        ! ##############################################################
        LOGICAL FUNCTION  IS_ERROR_CODE (error_code)
          IMPLICIT NONE
          
          INTEGER, INTENT(IN) :: error_code
          
          IS_ERROR_CODE = IS_LAST_ERROR_CODE(error_code)

        END FUNCTION IS_ERROR_CODE



        ! ##############################################################
        !+
        ! Compare the input error code with the current error code.
        !-
        ! ##############################################################
        LOGICAL FUNCTION  IS_LAST_ERROR_CODE (error_code)
          IMPLICIT NONE
          
          INTEGER, INTENT(IN) :: error_code
          
          IF (m_error) THEN
             IS_LAST_ERROR_CODE = IS_INDEX_ERROR_CODE(error_code, m_stack_position)
          ELSE
             IS_LAST_ERROR_CODE = (error_code .EQ. 0)
          ENDIF

        END FUNCTION IS_LAST_ERROR_CODE


        ! ##############################################################
        !+
        ! Compare the input error code with the current error code.
        !-
        ! ##############################################################
        LOGICAL FUNCTION  IS_FIRST_ERROR_CODE (error_code)
          IMPLICIT NONE
          
          INTEGER, INTENT(IN) :: error_code
          
          IF (m_error) THEN
             IS_FIRST_ERROR_CODE = IS_INDEX_ERROR_CODE(error_code, 1)
          ELSE
             IS_FIRST_ERROR_CODE = (error_code .EQ. 0)
          ENDIF

        END FUNCTION IS_FIRST_ERROR_CODE


        ! ##############################################################
        !+
        ! Compare the input error code with the current error code.
        !-
        ! ##############################################################
        LOGICAL FUNCTION  IS_ANY_ERROR_CODE (error_code)
          IMPLICIT NONE
          
          INTEGER, INTENT(IN) :: error_code

          INTEGER :: ii
          
          IF (m_error) THEN
             DO ii=1,m_error_stack_size
                IS_ANY_ERROR_CODE = IS_INDEX_ERROR_CODE(error_code, ii)
                IF (IS_ANY_ERROR_CODE) RETURN
             END DO
          ELSE
             IS_ANY_ERROR_CODE = (error_code .EQ. 0)
          ENDIF

        END FUNCTION IS_ANY_ERROR_CODE


        ! ##############################################################
        !+
        ! Compare the input error code with the error code at the
        ! given position in the stack.
        !
        ! Note: This will not check the stack position or the
        !       error status.
        !-
        ! ##############################################################
        LOGICAL FUNCTION  IS_INDEX_ERROR_CODE (error_code, index)
          IMPLICIT NONE
          
          INTEGER, INTENT(IN) :: error_code
          INTEGER, INTENT(IN) :: index
          
          IS_INDEX_ERROR_CODE = (error_code .EQ. m_error_code(index))

        END FUNCTION IS_INDEX_ERROR_CODE



        ! ##############################################################
        !+
        ! Print the last error location & message if error exists.
        !
        ! This is a convience routine, it simply wraps:
        !   PRINT_LAST_ERROR
        !-
        ! ##############################################################
        SUBROUTINE PRINT_ERROR ()
          IMPLICIT NONE
          
          CALL PRINT_LAST_ERROR()

        END SUBROUTINE PRINT_ERROR



        ! ##############################################################
        !+
        ! Print the last error location & message if error exists.
        !-
        ! ##############################################################
        SUBROUTINE PRINT_LAST_ERROR ()
          IMPLICIT NONE
          
          IF (m_error) THEN
             CALL PRINT_INDEX_ERROR(MIN(m_stack_position, m_error_stack_size))
          ENDIF

        END SUBROUTINE PRINT_LAST_ERROR



        ! ##############################################################
        !+
        ! Print the first error location & message if error exists.
        !-
        ! ##############################################################
        SUBROUTINE PRINT_FIRST_ERROR ()
          IMPLICIT NONE
          
          IF (m_error) THEN
             CALL PRINT_INDEX_ERROR(1)
          ENDIF

        END SUBROUTINE PRINT_FIRST_ERROR



        ! ##############################################################
        !+
        ! Print the all error locations & messages if error exists.
        !-
        ! ##############################################################
        SUBROUTINE PRINT_ALL_ERROR ()
          IMPLICIT NONE
          
          INTEGER :: ii

          IF (m_error) THEN
             DO ii=1,MIN(m_stack_position, m_error_stack_size)
                CALL PRINT_INDEX_ERROR(ii)
             ENDDO
          ENDIF

        END SUBROUTINE PRINT_ALL_ERROR



        ! ##############################################################
        !+
        ! Print the error location & message at the given index 
        ! in the stack.
        !
        ! Note: This will not check the stack position or the
        !       error status.
        !-
        ! ##############################################################
        SUBROUTINE PRINT_INDEX_ERROR (index)
          IMPLICIT NONE
          
          INTEGER, INTENT(IN) :: index

          WRITE(*,'(a,a,": ",a)') 'ERROR: ', TRIM(m_error_location(index)) &
               ,TRIM(m_error_message(index))

        END SUBROUTINE PRINT_INDEX_ERROR




        ! ##############################################################
        !+
        ! Print the all error codes in the stack if error exists.
        !-
        ! ##############################################################
        SUBROUTINE PRINT_ALL_ERROR_CODE ()
          IMPLICIT NONE
          
          INTEGER :: ii

          IF (m_error) THEN
             DO ii=1,MIN(m_stack_position, m_error_stack_size)
                CALL PRINT_INDEX_ERROR_CODE(ii)
             ENDDO
          ENDIF

        END SUBROUTINE PRINT_ALL_ERROR_CODE



        ! ##############################################################
        !+
        ! Print the error location & message at the given index 
        ! in the stack.
        !
        ! Note: This will not check the stack position or the
        !       error status.
        !-
        ! ##############################################################
        SUBROUTINE PRINT_INDEX_ERROR_CODE (index)
          IMPLICIT NONE
          
          INTEGER, INTENT(IN) :: index

          WRITE(*,'(a,i0)') 'ERROR CODE: ', m_error_code(index)

        END SUBROUTINE PRINT_INDEX_ERROR_CODE




        ! ##############################################################
        !+
        ! Print the last error type if error exists.
        !
        ! This is a convience routine, it simply wraps:
        !   PRINT_LAST_ERROR_TYPE
        !-
        ! ##############################################################
        SUBROUTINE PRINT_ERROR_TYPE ()
          IMPLICIT NONE
          
          CALL PRINT_LAST_ERROR_TYPE()

        END SUBROUTINE PRINT_ERROR_TYPE



        ! ##############################################################
        !+
        ! Print the last error type if error exists.
        !-
        ! ##############################################################
        SUBROUTINE PRINT_LAST_ERROR_TYPE ()
          IMPLICIT NONE
          
          IF (m_error) THEN
             CALL PRINT_INDEX_ERROR_TYPE(MIN(m_stack_position, m_error_stack_size))
          ENDIF

        END SUBROUTINE PRINT_LAST_ERROR_TYPE



        ! ##############################################################
        !+
        ! Print the first error type if error exists.
        !-
        ! ##############################################################
        SUBROUTINE PRINT_FIRST_ERROR_TYPE ()
          IMPLICIT NONE
          
          IF (m_error) THEN
             CALL PRINT_INDEX_ERROR_TYPE(1)
          ENDIF

        END SUBROUTINE PRINT_FIRST_ERROR_TYPE



        ! ##############################################################
        !+
        ! Print the all error types if error exists.
        !-
        ! ##############################################################
        SUBROUTINE PRINT_ALL_ERROR_TYPE ()
          IMPLICIT NONE
          
          INTEGER :: ii

          IF (m_error) THEN
             DO ii=1,m_stack_position
                CALL PRINT_INDEX_ERROR_TYPE(ii)
             ENDDO
          ENDIF

        END SUBROUTINE PRINT_ALL_ERROR_TYPE



        ! ##############################################################
        !+
        ! Print the error type at the given index in the stack.
        !
        ! Note: This will not check the stack position or the
        !       error status.
        !-
        ! ##############################################################
        SUBROUTINE PRINT_INDEX_ERROR_TYPE (index)
          IMPLICIT NONE
          
          INTEGER, INTENT(IN) :: index

          WRITE(*,'(a,a)') 'ERROR TYPE: ', TRIM(m_error_type(index))

        END SUBROUTINE PRINT_INDEX_ERROR_TYPE



        ! ##############################################################
        !+
        ! Turn on or off debug mode.
        ! 
        ! In debug mode all errors will be printed when they are set.
        !-
        ! ##############################################################
        SUBROUTINE SET_ERROR_DEBUG (status)
          IMPLICIT NONE
          
          LOGICAL, INTENT(IN) :: status
          m_error_debug = status

        END SUBROUTINE SET_ERROR_DEBUG

      END MODULE error_module
