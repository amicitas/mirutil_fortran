



! ======================================================================
!+
! PURPOSE:
!   A module to control debugging modes for CERFIT.  
!    
! DESRIPTION:
!   This module allows debug actions to be made conditional on a
!   a variety of debug modes being active.
!
!   The active debug modes can be set by the user.
!
!   A convience funciton is also included for the dispaly of debug
!   messages.
!
! PROGRAMING NOTES:
!   I have chosen to base the debug mode on integer debug codes instead
!   of string codes.  This is to ensure that checking a debug mode can 
!   be done very fast.
!
!
! CHANGE LOG:
!   Written: 2010-03-11: Novimir Antoniuk Pablant
!
!-
! ======================================================================

      MODULE debug_module
        PRIVATE

        PUBLIC :: DEBUG &
                  ,SET_DEBUG &
                  ,DEBUG_SET_DEFAULTS &
                  ,PRINT_DEBUG &
                  ,PRINT_DEBUG_MESSAGE

        LOGICAL, PUBLIC :: DEB = .false.

        ! --------------------------------------------------------------
        ! Define the available debuging modes.
        INTEGER, PARAMETER, PUBLIC :: debugDefault = 1
        INTEGER, PARAMETER, PUBLIC :: debugDebug = 2
        INTEGER, PARAMETER, PUBLIC :: debugInfo = 3
        INTEGER, PARAMETER, PUBLIC :: debugWarning = 4
        INTEGER, PARAMETER, PUBLIC :: debugDepretiated = 5

        INTEGER, PARAMETER :: m_num_debug_modes = 5


        ! --------------------------------------------------------------
        ! Some convenience variables.
        INTEGER, PARAMETER :: m_debug_string_len = 100
        CHARACTER(LEN=m_debug_string_len), PUBLIC :: m_debug_string


        ! Define interal module variables.
        LOGICAL, SAVE, DIMENSION(m_num_debug_modes) :: m_active_debug = .FALSE.


      CONTAINS
                
        
        
        ! ==============================================================
        !+
        ! Set the default debug flags for CERFIT.
        !-
        ! ==============================================================
        SUBROUTINE DEBUG_SET_DEFAULTS()
          IMPLICIT NONE
          
          m_active_debug = .FALSE.
          m_active_debug(debugInfo) = .TRUE.
          m_active_debug(debugWarning) = .TRUE.
          m_active_debug(debugDepretiated) = .TRUE.
          
        END SUBROUTINE DEBUG_SET_DEFAULTS

                
        
        
        ! ==============================================================
        !+
        ! Set the default debug flags for CERFIT.
        !-
        ! ==============================================================
        SUBROUTINE SET_DEBUG(debug_code, status)
          IMPLICIT NONE
          
          INTEGER, INTENT(IN) :: debug_code
          LOGICAL, INTENT(IN) :: status
          
          IF ((debug_code .GT. m_num_debug_modes) .OR. (debug_code .LT. 1)) THEN
             WRITE(*,*) 'DEBUG: Invalid debug code given.'
             RETURN
          ENDIF
          
          m_active_debug(debug_code) = status
          
        END SUBROUTINE SET_DEBUG


        ! ==============================================================
        !+
        ! Check the debug level.  Will return .TRUE. if debug output
        ! sould be produced for the given level.
        !-
        ! ==============================================================
        FUNCTION DEBUG (debug_code)
          IMPLICIT NONE
          
          LOGICAL :: DEBUG
          INTEGER, INTENT(IN), OPTIONAL :: debug_code
          INTEGER :: debug_code_local

          debug_code_local = 1

          IF (PRESENT(debug_code)) THEN
             debug_code_local = debug_code
          
             IF ((debug_code_local .GT. m_num_debug_modes) .OR. (debug_code_local .LT. 1)) THEN
                WRITE(*,*) 'DEBUG: Invalid debug code given.'
                DEBUG = .FALSE.
                RETURN
             ENDIF
          ENDIF
          
          DEBUG = m_active_debug(debug_code_local)      
          RETURN
          
        END FUNCTION DEBUG
        
        
        
        ! ==============================================================
        !+
        ! Check the debug level, and if appropriate, print the 
        ! debug message.
        !-
        ! ==============================================================
        SUBROUTINE PRINT_DEBUG(location, debug_message, debug_code)
          IMPLICIT NONE
          
          CHARACTER(LEN=*), INTENT(IN) ::location
          CHARACTER(LEN=*), INTENT(IN) :: debug_message

          INTEGER, INTENT(IN), OPTIONAL :: debug_code
          INTEGER :: debug_code_local = 1

       
          ! Check the debug level.
          IF (PRESENT(debug_code)) THEN
             debug_code_local = debug_code
          ENDIF
          IF (.NOT. DEBUG(debug_code_local)) RETURN

          CALL PRINT_DEBUG_MESSAGE(location, debug_message)
          
        END SUBROUTINE PRINT_DEBUG

        
        
        
        ! ==============================================================
        !+
        ! PURPOSE:
        !   Write the debug message using a common format.
        !  
        !   This function can be used to make all debug messages have
        !   a common format even when the use of <PRINT_DEBUG> is
        !   not appropriate
        !-
        ! ==============================================================
        SUBROUTINE PRINT_DEBUG_MESSAGE(location, debug_message)
          IMPLICIT NONE
          
          CHARACTER(LEN=*), INTENT(IN) ::location
          CHARACTER(LEN=*), INTENT(IN) :: debug_message

          ! Display the given message.
          WRITE(*,'(a,a,": ",a)') 'DEBUG: ', TRIM(location) &
               ,TRIM(debug_message)
          
        END SUBROUTINE PRINT_DEBUG_MESSAGE



        
      END MODULE debug_module

      !
