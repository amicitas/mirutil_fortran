



! ======================================================================
!+
!
! This is a module to deal with pathnames.
!
! This is meant to eventually replace the PATHNAMES module
!   (lib/cerdata/pathnames.f90)
!
! IMPORTANT:  For now this only supports POSIX style path names.
!
!-
! ======================================================================
      MODULE path_utilities
        USE error_module
        IMPLICIT NONE

        PRIVATE

        PUBLIC :: PATH_JOIN &
                  ,PATH_IS_ABSOLUTE &
                  ,PATH_GET_FILEHEAD &
                  ,PATH_GET_FILETAIL &
                  ,PATH_GET_FILENAME &
                  ,m_max_filename_length &
                  ,m_max_path_length

        ! The maximum length of a filename string.
        ! This is an actual limit for most file systems.
        INTEGER, PARAMETER :: m_max_filename_length = 255

        ! The maximum length of a path string.
        ! This is an arbitrary limit.  
        ! There are in general no file system limits.
        INTEGER, PARAMETER :: m_max_path_length = 255

      CONTAINS



        ! ##############################################################
        !+
        ! Join two paths.
        !
        ! This function will make sure that there is one, and only one,
        ! path separator between the paths.
        !
        ! It will handle blank paths.
        ! It will also check that the second path to be joined 
        ! is not an absolute path.
        !
        ! Assume everything is for POSIX.
        !-
        ! ##############################################################
        FUNCTION PATH_JOIN(path_1, path_2)
          IMPLICIT NONE

          CHARACTER(LEN=*), INTENT(IN) :: path_1
          CHARACTER(LEN=*), INTENT(IN) :: path_2

          ! This is the maximum length of the final path.
          ! This will often result in one blank character at the end.
          CHARACTER(LEN=(LEN(TRIM(path_1))+LEN(TRIM(path_2))+1)) :: PATH_JOIN


          INTEGER :: path_1_length, path_2_length

          path_1_length = LEN(TRIM(path_1))
          path_2_length = LEN(TRIM(path_2))

          ! First check if the fill path exists.
          IF (path_1_length .EQ. 0) THEN
             PATH_JOIN = path_2
             RETURN
          ENDIF

          ! Check the last character of path_1
          IF (path_1(path_1_length:path_1_length) .EQ. PATH_SEPARATOR()) THEN
             path_1_length = path_1_length - 1
          ENDIF

          ! Check if the second path exists.
          IF (path_2_length .EQ. 0) THEN
             PATH_JOIN = path_1(1:path_1_length)
             RETURN
          END IF

          ! Check the first character of path_2
          IF (path_2(1:1) .EQ. PATH_SEPARATOR()) THEN
             CALL SET_ERROR('PATH_JOIN', 'Attempt to append absolute path.', errorPathInvalid)
             RETURN
          ENDIF



          ! Now join the two paths
          PATH_JOIN = path_1(1:path_1_length) // PATH_SEPARATOR() // path_2(1:path_2_length)

        END FUNCTION PATH_JOIN



        ! ##############################################################
        !+
        ! Return the path separator appropriate to the system.
        !
        ! For now just return the POSIX separator.
        !-
        ! ##############################################################
        FUNCTION PATH_SEPARATOR()
          IMPLICIT NONE
          CHARACTER(LEN=1) :: PATH_SEPARATOR
          
          PATH_SEPARATOR = '/'

        END FUNCTION PATH_SEPARATOR



        ! ##############################################################
        !+
        !
        ! Check to see if the given path is an absolute path.
        ! Absolute paths start with '/'.
        !
        !-
        ! ##############################################################
        FUNCTION PATH_IS_ABSOLUTE(path)
          IMPLICIT NONE

          LOGICAL :: PATH_IS_ABSOLUTE

          CHARACTER(LEN=*), INTENT(IN) :: path

          INTEGER :: char_index

          char_index = INDEX(path, PATH_SEPARATOR())

          IF (char_index .EQ. 1) THEN
             PATH_IS_ABSOLUTE = .TRUE.
          ELSE
             PATH_IS_ABSOLUTE = .FALSE.
          ENDIF
          
        END FUNCTION PATH_IS_ABSOLUTE


        ! ##############################################################
        !+
        ! Return the filename from a given path
        ! See the file header for a definiton of terms.
        !-
        ! ##############################################################
        FUNCTION PATH_GET_FILENAME(path)
          IMPLICIT NONE

          CHARACTER(LEN=*), INTENT(IN) :: path

          CHARACTER(LEN=LEN(path)) :: PATH_GET_FILENAME

          INTEGER :: char_index

          ! Now find the last '/'
          char_index = INDEX(path, PATH_SEPARATOR(), .TRUE.)
          
          IF (char_index .EQ. LEN(path)) THEN
             PATH_GET_FILENAME = ''
          ELSE
             PATH_GET_FILENAME = path(char_index+1:)
          ENDIF

        END FUNCTION PATH_GET_FILENAME


        ! ##############################################################
        !+
        ! Return the head of the given file.
        ! See the file header for a definiton of terms.
        !-
        ! ##############################################################
        FUNCTION PATH_GET_FILEHEAD(path)
          IMPLICIT NONE

          CHARACTER(LEN=*), INTENT(IN) :: path

          CHARACTER(LEN=LEN(path)) :: PATH_GET_FILEHEAD
          CHARACTER(LEN=LEN(path)) :: filename

          INTEGER :: char_index

          ! First get the filename.
          filename = PATH_GET_FILENAME(path)

          ! Now find the last '.'
          char_index = INDEX(path, '.', .TRUE.)
          
          IF (char_index .LE. 1) THEN
             PATH_GET_FILEHEAD = ''
          ELSE
             PATH_GET_FILEHEAD = path(1:char_index-1)
          END IF

        END FUNCTION PATH_GET_FILEHEAD



        ! ##############################################################
        !+
        ! Return the tail of the given file.
        ! See the file header for a definiton of terms.
        !-
        ! ##############################################################
        FUNCTION PATH_GET_FILETAIL(path)
          IMPLICIT NONE

          CHARACTER(LEN=*), INTENT(IN) :: path

          CHARACTER(LEN=LEN(path)) :: PATH_GET_FILETAIL
          CHARACTER(LEN=LEN(path)) :: filename

          INTEGER :: char_index

          ! First get the filename.
          filename = PATH_GET_FILENAME(path)

          ! Now find the last '.'
          char_index = INDEX(path, '.', .TRUE.)
          
        
          IF ((char_index .EQ. LEN(path)) .OR. (char_index .LT. 1)) THEN
             PATH_GET_FILETAIL = ''
          ELSE
             PATH_GET_FILETAIL = path(char_index+1:)
          ENDIF

        END FUNCTION PATH_GET_FILETAIL


      END MODULE path_utilities
