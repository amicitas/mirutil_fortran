



! ======================================================================
!+
!
! This is a module contains a number of string utilites
!
!-
! ======================================================================
      MODULE string_utilities
        IMPLICIT NONE

        PRIVATE

        ! Find the index of the upper and lower case letters as this 
        ! may change between processors
        INTEGER, PARAMETER :: m_ascii_pos_lower_a = IACHAR('a')
        INTEGER, PARAMETER :: m_ascii_pos_upper_a = IACHAR('A')

        INTEGER, PARAMETER :: m_ascii_pos_lower_z = IACHAR('z')
        INTEGER, PARAMETER :: m_ascii_pos_upper_z = IACHAR('Z')

        PUBLIC :: STRING_UPPERCASE, STRING_LOWERCASE &
                  ,STRING_REMOVE_CHARS &
                  ,STRING_FIND_ANY &
                  ,STRING_FIND_NOT
      CONTAINS


        ! ##############################################################
        !+
        ! Convert a string to UPPERCASE.
        !
        ! This will only work for ASCII characters.
        !-
        ! ##############################################################
        FUNCTION STRING_UPPERCASE (string)
          IMPLICIT NONE

          CHARACTER (LEN=*), INTENT(IN) :: string
          CHARACTER (LEN=LEN(string)) :: STRING_UPPERCASE

          INTEGER :: ascii_value
          INTEGER :: ii

          STRING_UPPERCASE = string

          DO ii=1, LEN(STRING_UPPERCASE)
             ascii_value = IACHAR(STRING_UPPERCASE(ii:ii))
             IF ((ascii_value .GE. m_ascii_pos_lower_a) &
                  .AND. (ascii_value .LE. m_ascii_pos_lower_z)) THEN

                STRING_UPPERCASE(ii:ii) = &
                     ACHAR(ascii_value - (m_ascii_pos_lower_a - m_ascii_pos_upper_a))
             ENDIF
          ENDDO

        END FUNCTION STRING_UPPERCASE




        ! ##############################################################
        !+
        ! Convert a string to UPPERCASE.
        !
        ! This will only work for ASCII characters.
        !
        ! NOTE: This is just as fast as the CERFIT routine TOLOWR
        !-
        ! ##############################################################
        FUNCTION STRING_LOWERCASE (string)
          IMPLICIT NONE

          CHARACTER (LEN=*), INTENT(IN) :: string
          CHARACTER (LEN=LEN(string)) :: STRING_LOWERCASE

          INTEGER :: ascii_value
          INTEGER :: ii

          STRING_LOWERCASE = string

          DO ii=1, LEN(STRING_LOWERCASE)
             ascii_value = IACHAR(STRING_LOWERCASE(ii:ii))
             IF ((ascii_value .GE. m_ascii_pos_upper_a) &
                  .AND. (ascii_value .LE. m_ascii_pos_upper_z)) THEN

                STRING_LOWERCASE(ii:ii) = &
                     ACHAR(ascii_value + (m_ascii_pos_lower_a - m_ascii_pos_upper_a))
             ENDIF
          ENDDO

        END FUNCTION STRING_LOWERCASE




        ! ##############################################################
        !+
        ! Remove a set of characters from a string.
        !
        !
        ! CHANGE LOG:
        !   Written: 2009-05-13 by Novimir Antoniuk Pablant
        !-
        ! ##############################################################
        FUNCTION STRING_REMOVE_CHARS (string, chars_remove)
          IMPLICIT NONE

          CHARACTER (LEN=*), INTENT(IN) :: string
          CHARACTER (LEN=*), INTENT(IN) :: chars_remove

          CHARACTER (LEN=LEN(string)) :: STRING_REMOVE_CHARS

          INTEGER :: char_length
          LOGICAL :: char_match

          INTEGER :: ii, jj, current_position


          STRING_REMOVE_CHARS = ''
          char_length = LEN(chars_remove)
          current_position = 1
          

          DO ii = 1,LEN(string)
             char_match = .FALSE.
             DO jj = 1,char_length
                IF (string(ii:ii) .EQ. chars_remove(jj:jj)) THEN
                   char_match = .TRUE.
                   EXIT
                ENDIF
             END DO
             IF (.NOT. char_match) THEN
                STRING_REMOVE_CHARS(current_position:current_position) = &
                     string(ii:ii)
                current_position = current_position + 1
             END IF
          END DO
        END FUNCTION STRING_REMOVE_CHARS




        ! ##############################################################
        !+
        !
        ! Search the given string and find any of the given characters.
        ! 
        !
        ! CHANGE LOG:
        !   Written: 2009-09-08 by Novimir Antoniuk Pablant
        !-
        ! ##############################################################
        FUNCTION STRING_FIND_ANY (string, char_to_find)
          IMPLICIT NONE

          CHARACTER (LEN=*), INTENT(IN) :: string
          CHARACTER (LEN=*), INTENT(IN) :: char_to_find

          INTEGER :: STRING_FIND_ANY

          INTEGER :: len_string
          INTEGER :: len_char_to_find

          INTEGER :: ii, jj

          STRING_FIND_ANY = 0

          len_string = LEN(string)
          len_char_to_find = LEN(char_to_find)


          ! If either input string is the null string,
          ! then just return 0.
          IF (len_char_to_find .EQ. 0) RETURN
          IF (len_string .EQ. 0) RETURN
          
          DO ii=1, len_string
             DO jj=1,len_char_to_find

                ! Do the actuall search.
                IF (string(ii:ii) .EQ. char_to_find(jj:jj)) THEN
                   STRING_FIND_ANY = ii
                   RETURN
                ENDIF
             END DO
          END DO

        END FUNCTION STRING_FIND_ANY





        ! ##############################################################
        !+
        !
        ! Search the given string and find any character that is
        ! not one of the given characters.
        ! 
        ! CHANGE LOG:
        !   Written: 2009-09-08 by Novimir Antoniuk Pablant
        !-
        ! ##############################################################
        FUNCTION STRING_FIND_NOT (string, char_to_find)
          IMPLICIT NONE

          CHARACTER (LEN=*), INTENT(IN) :: string
          CHARACTER (LEN=*), INTENT(IN) :: char_to_find

          INTEGER :: STRING_FIND_NOT

          INTEGER :: len_string
          INTEGER :: len_char_to_find

          INTEGER :: ii, jj

          STRING_FIND_NOT = 0

          len_string = LEN(string)
          len_char_to_find = LEN(char_to_find)


          ! If either input string is the null string,
          ! then just return 0.
          IF (len_char_to_find .EQ. 0) RETURN
          IF (len_string .EQ. 0) RETURN
          
          DO ii=1, len_string
             DO jj=1,len_char_to_find

                ! Do the actuall search.
                IF (string(ii:ii) .NE. char_to_find(jj:jj)) THEN
                   STRING_FIND_NOT = ii
                   RETURN
                ENDIF
             END DO
          END DO

        END FUNCTION STRING_FIND_NOT




      END MODULE string_utilities
