


! ======================================================================
!+
! AUTHOR:
!   Novimir Antoniuk Pablant
!
! DATE:
!   2009-09
!
! PURPOSE:
!   This module hold utilities for dealing with data types.
!
!-
! ======================================================================
      MODULE type_utilities
        USE error_module
        IMPLICIT NONE

        PRIVATE
        
        PUBLIC :: LOGICAL_TO_INT &
                  ,INT_TO_LOGICAL

      CONTAINS


        
        ! ##############################################################
        !+
        ! 
        ! Convert a logical value to an integer.
        !   .TRUE. = 1
        !   .FALSE. = 0
        !
        ! In general this coversion is done by directly copying the
        ! bits between the two types,  making the result processor 
        ! dependant. This is true for  both for implicit conversions and 
        ! when using INT().
        !
        ! This function will ensure that the conversion is done
        ! in a consistant way.
        !
        !-
        ! ##############################################################
        FUNCTION LOGICAL_TO_INT(logical)
          IMPLICIT NONE

          INTEGER :: LOGICAL_TO_INT

          LOGICAL, INTENT(IN) :: logical

          IF (logical) THEN
             LOGICAL_TO_INT = 1
          ELSE
             LOGICAL_TO_INT = 0
          ENDIF

        END FUNCTION LOGICAL_TO_INT


        
        ! ##############################################################
        !+
        ! 
        ! Convert a an integer to a logical value.
        !   0 = .FALSE.
        !   NOT 0 = .TRUE.
        !
        ! In general this coversion is done by directly copying the
        ! bits between the two types,  making the result processor 
        ! dependant. This is true for  both for implicit conversions and 
        ! when using INT().
        !
        ! This function will ensure that the conversion is done
        ! in a consistant way.
        !
        !-
        ! ##############################################################
        FUNCTION INT_TO_LOGICAL(integer)
          IMPLICIT NONE

          LOGICAL :: INT_TO_LOGICAL
          
          INTEGER, INTENT(IN) :: integer

          IF (integer .EQ. 0) THEN
            INT_TO_LOGICAL  = .FALSE.
          ELSE
            INT_TO_LOGICAL = .TRUE.
          ENDIF

        END FUNCTION INT_TO_LOGICAL


      END MODULE type_utilities

      !
